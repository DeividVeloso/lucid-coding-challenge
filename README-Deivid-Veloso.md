Hello dear, my name is Deivid Veloso and I'm from São Paulo, Brazil.

I have been working with development for almost 8 years.

I know that we can always improve our code so I'm opened to improve and learn.

For this project there are some points that can be improved:

 - Coverage percentage
 - Adding some Placeholder/Sckelecton components for loading states
 - Configuring better `theme` from `Material-UI` to cover fonts, colors, spacing, media queries.
 - Adding more unit, integration and e2e tests.
 - Replace all JSS styles to use Styled-components.

 ## OBS
 I used the `Roboto` font for the components.

### Open source project that I'm working right now for my current company.

 - https://github.com/team-telnyx/webrtc
 
### Git flow
I used the feature branch workflow to work on this project.
![feature branch](./screenshots/feature-branch1.png)
### Third-party libraries installed by me
 - styled-components
 - @material-ui/lab
 - @testing-library/react
 - @testing-library/user-event
 - cypress
 - react-test-renderer
 - lodash (shuffle)

## How to run the project
1 - Run `yarn && yarn start`

2 - You should access `http://localhost:3000/`

![running-project](./screenshots/project.gif)

## How to run unit tests
1 - Run `yarn jest`

![coverage](./screenshots/unit-tests1.png)

## How to run coverage
1 - Run `yarn coverage`

2 - Navigate to the `coverage/lcov-report` folder

3 - Open the `index.html`

![coverage](./screenshots/coverage1.png)

## How to run e2e tests
1 - Run `yarn cypress:open`

![cypress](./screenshots/cypress1.png)
