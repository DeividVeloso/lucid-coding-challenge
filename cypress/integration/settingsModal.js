describe("SettingsModal", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("should render home page", () => {
    const title = cy.get("h1");
    title.should("exist");
    title.contains("Welcome to the Lucid Quiz");
    cy.get('[data-testid="settings"]').should("exist");
    cy.get('[data-testid="btn-start-quiz"]').should("exist");
    cy.get('[data-testid="btn-settings"]').should("exist");
  });

  it("should open and close Settings modal", () => {
    cy.get('[data-testid="settings"]').should("exist");
    cy.intercept("GET", "http://localhost:4000/api/questions", {
      response_code: 0,
      results: [
        {
          category: "Entertainment: Video Games",
          type: "multiple",
          difficulty: "easy",
          question:
            "Which game did &quot;Sonic The Hedgehog&quot; make his first appearance in?",
          correct_answer: "Rad Mobile",
          incorrect_answers: [
            "Sonic The Hedgehog",
            "Super Mario 64",
            "Mega Man",
          ],
        },
      ],
    }).as("getQuestions");
    cy.get('[data-testid="btn-settings"]')
      .should("be.visible")
      .then((e) => {
        Cypress.$(e).click();
      });
    cy.get('[data-testid="settings-modal"]').should("exist");
    cy.get('[data-testid="btn-save-seetings"]').should("exist");
    cy.get('[data-testid="btn-save-seetings"]').click();
  });
});
