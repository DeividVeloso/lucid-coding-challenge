const express = require("express");
const cors = require("cors");

const data = require("./data.json");

// create server
const server = express();
const port = 4000;
server.use(
  cors({
    origin: "*",
  })
);

// GET question endpoint
server.get("/api/questions", (req, res) => {
  res.json(data);
});

// starting server
server.listen(port, () => {
  console.log(`Server listening at ${port}`);
});
