import React from "react";
import Button from "@material-ui/core/Button";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import styled from 'styled-components'
import _shuffe from 'lodash/shuffle';

import { useQuestions } from "../../contexts/QuestionsContext";

import { useHistory } from "react-router-dom";
import FormSettings from "../FormSettings";




const ModalStyle = styled(Modal)`
  display: flex;
  align-items: center;
  justify-content: center;
`

const ContainerModal = styled.div`
  padding: 24px; 
  min-width: 320px; 
  max-height: 800px;
  overflow-y: auto;

  @media only screen and (max-width: 900px) {
    min-width: 320px; 
    max-height: 500px;
  }
`;

function Settings() {
  const history = useHistory();
  const {questions, setQuestions} = useQuestions();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div data-testid="settings" style={{ display: "flex", justifyContent: "center" }}>
      <ModalStyle
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Paper elevation={3} >
            <ContainerModal data-testid="settings-modal">
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <Typography
                  variant="h5"
                  color="textPrimary"
                  style={{ fontWeight: "bold" }}
                  gutterBottom
                >
                  Quiz Settings
                </Typography>

                <Button
                  color="default"
                  variant="text"
                  onClick={handleClose}
                  size="small"
                >
                  X
                </Button>
              </div>
              <Divider />
              <FormSettings onClose={handleClose} />
            </ContainerModal>
          </Paper>
        </Fade>
      </ModalStyle>
      <Button
        data-testid="btn-start-quiz"
        style={{ marginRight: 20 }}
        color="primary"
        variant="outlined"
        onClick={() => {
          const shuffledQuestions = _shuffe(questions);
          setQuestions(shuffledQuestions)
          history.push("/quiz");
        }}
      >
        Start Quiz
      </Button>
      <Button 
      data-testid="btn-settings"
      color="primary" variant="contained" onClick={handleOpen}>
        Settings
      </Button>
    </div>
  );
}
export default Settings;
