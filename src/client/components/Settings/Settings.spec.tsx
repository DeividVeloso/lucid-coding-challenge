import React from "react";
import Settings from "./index";

import { cleanup, render, screen } from "@testing-library/react";
import userEvent from '@testing-library/user-event';

describe("Settings Component", () => {
  afterEach(cleanup);

  it('it should render "Settings"', () => {
    const { debug } = render(<Settings />);

    const startButton = screen.getByRole('button', {
      name: /start quiz/i
    })

    const settingsButton = screen.getByRole("button", {
      name: /settings/i,
    });

    expect(startButton).not.toBeNull();
    expect(settingsButton).not.toBeNull();

  });

  it('it should render "Settings" and open and close the Modal', () => {
    const { debug , rerender } = render(<Settings />);

    const settingsButton = screen.getByRole("button", {
      name: /settings/i,
    });

    expect(settingsButton).not.toBeNull();

    userEvent.click(settingsButton);
   
    rerender(<Settings />);
    const modal = screen.getByTestId('settings-modal')
    expect(modal).not.toBeNull();

    const modalTitle = screen.getByRole('heading', {
      name: /quiz settings/i
    })
    expect(modalTitle.textContent).toEqual("Quiz Settings");

    const closeModalButton = screen.getByRole('button', {
      name: /x/i
    });

    userEvent.click(closeModalButton);
  });
});
