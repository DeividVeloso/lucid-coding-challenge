import React, { ChangeEvent } from "react";
import TextField from "@material-ui/core/TextField";
import { Typography } from "@material-ui/core";
import Title from "../common/Title";
interface Question {
  id: string;
  name: string;
  label: string;
  value: any;
  onChange: (event: ChangeEvent<HTMLInputElement>, value: string) => void;
  field: any;
  form: any;
}

export default function TextInput({
  id,
  label,
  field: { name, onChange, value = "" },
  form,
}: Question) {
  return (
    <React.Fragment>
      <Title label={label} />
      <TextField
        id={id}
        name={name}
        value={value}
        onChange={onChange}
        variant="outlined"
        fullWidth
        style={{width: '80%'}}
      />
      {form.errors[name] && (
        <Typography color="error">{form.errors[name]}</Typography>
      )}
    </React.Fragment>
  );
}

