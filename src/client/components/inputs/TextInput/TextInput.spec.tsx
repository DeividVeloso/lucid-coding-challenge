import React from "react";
import TextInput from "./index";
import { cleanup, render, screen } from "@testing-library/react";
 

describe("TextInput Component", () => {
  afterEach(cleanup);

  it('it should render "TextInput"', () => {
    const value = 'Hello';
    const onChange = () => {};
    const field = {};
    const form = {
      errors: {
        textInputName: ''
      }
    };


    const { container} = render(
      <TextInput
        id="textInputId"
        name="textInputName"
        label="Choose one answer"
        value={value}
        onChange={onChange}
        field={field}
        form={form}
      />
    );
    const pageTitle = screen.getByText(/Choose one answer/i);
    expect(pageTitle).toBeTruthy();
    expect(pageTitle.textContent).toEqual("Choose one answer");

    const inputText = container.querySelectorAll('input')
    expect(inputText).toHaveLength(1)

  });

  it('it should render "TextInput" with error message', () => {
    const value = 'Hello';
    const onChange = () => {};
    const field = {
      name: 'textInputName'
    };
    const form = {
      errors: {
        textInputName: 'Error'
      }
    };

    render(
      <TextInput
        id="textInputId"
        name="textInputName"
        label="Choose one answer"
        value={value}
        onChange={onChange}
        field={field}
        form={form}
      />
    );

    const pageTitle = screen.getByText(/Error/i);
    expect(pageTitle).toBeTruthy();
    expect(pageTitle.textContent).toEqual("Error");
  })  
});
