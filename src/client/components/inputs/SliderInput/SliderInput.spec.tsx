import React from "react";
import SliderInput from "./index";
import { cleanup, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

describe("SliderInput Component", () => {
  afterEach(cleanup);

  it('it should render "SliderInput"', () => {
    const value = 1;
    const onChange = () => {};
    const field = {};
    const form = {
      errors: {
        multipleName: "",
      },
    };

    const { debug, container } = render(
      <SliderInput
        id="sliderId"
        name="sliderName"
        label="Choose the quantity"
        value={value}
        onChange={onChange}
        field={field}
        form={form}
      />
    );
    const pageTitle = screen.getByText(/Choose the quantity/i);
    expect(pageTitle).toBeTruthy();
    expect(pageTitle.textContent).toEqual("Choose the quantity");

    const inputSlider = container.querySelectorAll("input");
    expect(inputSlider).toHaveLength(1);
  });

  it('it should render "SliderInput" with error message', () => {
    const value = 1;
    const onChange = () => {};
    const field = {
      name: "sliderName",
    };
    const form = {
      errors: {
        sliderName: "Error",
      },
    };

    render(
      <SliderInput
        id="sliderId"
        name="sliderName"
        label="Choose the quantity"
        value={value}
        onChange={onChange}
        field={field}
        form={form}
      />
    );

    const pageTitle = screen.getByText(/Error/i);
    expect(pageTitle).toBeTruthy();
    expect(pageTitle.textContent).toEqual("Error");
  });

  it('it should render "SliderInput" with value equal 10', () => {
    let value = 1;
    const onChange = (event: any) => {
      value = 10;
    };
    const field = {};
    const form = {
      setFieldValue: onChange,
      errors: {
        multipleName: "",
      },
    };

    const { debug, container, rerender } = render(
      <SliderInput
        id="sliderId"
        name="sliderName"
        label="Choose the quantity"
        value={value}
        onChange={onChange}
        field={field}
        form={form}
      />
    );
    const pageTitle = screen.getByText(/Choose the quantity/i);
    expect(pageTitle).toBeTruthy();
    expect(pageTitle.textContent).toEqual("Choose the quantity");

    const inputSlider = container.querySelector("input");

    userEvent.type(inputSlider as HTMLInputElement, "10");

    rerender(
      <SliderInput
        id="sliderId"
        name="sliderName"
        label="Choose the quantity"
        value={value}
        onChange={onChange}
        field={field}
        form={form}
      />
    );

    expect(inputSlider!.value).toEqual("10");
  });
});
