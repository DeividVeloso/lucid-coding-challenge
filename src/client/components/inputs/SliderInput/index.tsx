import React, { ChangeEvent } from "react";
import Slider from "@material-ui/core/Slider";
import { Typography } from "@material-ui/core";
import Title from "../common/Title";
interface Question {
  id: string;
  name: string;
  label: string;
  value: any;
  onChange: (event: ChangeEvent<HTMLInputElement>, value: string) => void;
  field: any;
  form: any;
}

export default function TextInput({
  id,
  name,
  label,
  field,
  form,
  ...props
}: Question) {
  return (
    <div>
      <Title label={label} />
      <Slider
        {...field}
        {...props}
        id={id}
        name={name}
        onChange={(e, value) => {
          form.setFieldValue(field.name, value);
        }}
      />
      {form.errors[field.name] && (
        <Typography color="error">{form.errors[field.name]}</Typography>
      )}
    </div>
  );
}
