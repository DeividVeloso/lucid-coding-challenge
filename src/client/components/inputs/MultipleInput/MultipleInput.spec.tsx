import React from "react";
import MultipleInput from "./index";
import { cleanup, render, screen } from "@testing-library/react";
 

describe("MultipleInput Component", () => {
  afterEach(cleanup);

  it('it should render "MultipleInput"', () => {
    const value = '1';
    const onChange = () => {};
    const field = {};
    const form = {
      errors: {
        multipleName: ''
      }
    };


    const { container} = render(
      <MultipleInput
        id="multipleId"
        name="multipleName"
        label="Choose one answer"
        options={['1', '2', '3']}
        value={value}
        onChange={onChange}
        field={field}
        form={form}
      />
    );
    const pageTitle = screen.getByText(/Choose one answer/i);
    expect(pageTitle).toBeTruthy();
    expect(pageTitle.textContent).toEqual("Choose one answer");

    const inputRadio = container.querySelectorAll('input')
    expect(inputRadio).toHaveLength(3)

  });

  it('it should render "MultipleInput" with error message', () => {
    const value = '1';
    const onChange = () => {};
    const field = {
      name: 'multipleName'
    };
    const form = {
      errors: {
        multipleName: 'Error'
      }
    };

    render(
      <MultipleInput
        id="multipleId"
        name="multipleName"
        label="Choose one answer"
        options={['1', '2', '3']}
        value={value}
        onChange={onChange}
        field={field}
        form={form}
      />
    );

    const pageTitle = screen.getByText(/Error/i);
    expect(pageTitle).toBeTruthy();
    expect(pageTitle.textContent).toEqual("Error");
  })  
});
