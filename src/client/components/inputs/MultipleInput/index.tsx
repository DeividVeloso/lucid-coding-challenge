import React, { ChangeEvent } from "react";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import Typography from "@material-ui/core/Typography";
import Title from "../common/Title";

interface Question {
  options: Array<any>;
  id: string;
  name: string;
  label: string;
  value: any;
  onChange: (event: ChangeEvent<HTMLInputElement>, value: string) => void;
  field: any;
  form: any;
}

export default function MultipleInput({
  id,
  label,
  options,
  field: { name, onChange, value = "" },
  form,
}: Question) {
  function renderOptions() {
    return options.map((item) => {
      return (
        <FormControlLabel
          key={item}
          value={item}
          control={<Radio color="primary" />}
          label={<Typography color="textPrimary">{item}</Typography>}
        />
      );
    });
  }

  const renderedOptions = renderOptions();

  return (
    <FormControl component="fieldset" >
      <Title label={label} />
      <RadioGroup
        data-testid="mutiple-input-radio"
        id={id}
        name={name}
        aria-label={label}
        value={value}
        onChange={onChange}
      >
        {renderedOptions}
      </RadioGroup>

      {form.errors[name] && (
        <Typography color="error">{form.errors[name]}</Typography>
      )}
    </FormControl>
  );
}
