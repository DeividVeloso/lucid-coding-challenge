import React from "react";
import Typography from "@material-ui/core/Typography";
import styled from "styled-components";

const TitleTyppography = styled(Typography)`
  margin: 0px 0px 24px 0px;
`;

interface ITitle {
  label: string;
}

export default function Title({ label }: ITitle) {
  return (
    <TitleTyppography
      dangerouslySetInnerHTML={{ __html: label }}
      variant="h5"
      color="textPrimary"
    ></TitleTyppography>
  );
}
