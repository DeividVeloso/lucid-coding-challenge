import React from 'react';
import Title from "./index";
import { cleanup, render, screen } from "@testing-library/react";

describe("Title Component", () => {
  afterEach(cleanup);

  it('it should render "Title" with label equal "Hello"', () => {
    render(<Title label="Hello"/>);
    const pageTitle = screen.getByText(/Hello/i);
    expect(pageTitle).toBeTruthy();
    expect(pageTitle.textContent).toEqual("Hello");
  });
});
