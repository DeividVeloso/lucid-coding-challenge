import React from "react";
import styled from "styled-components";

interface IContainer {
  children: JSX.Element | JSX.Element[] | null;
}

const Wrapper = styled.main`
  display: flex;
  flex-direction: column;
  margin-top: 60px;
  margin-left: 16px;
  margin-right: 16px;
`;

function Container({ children }: IContainer) {
  return <Wrapper>{children}</Wrapper>;
}
export default Container;
