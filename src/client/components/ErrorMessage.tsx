import React from "react";
import styled from "styled-components";
import Typography from "@material-ui/core/Typography";

const ErrorContainer = styled.div`
  display: flex;
  justify-content: center;
`;

export default function ErrorMessage({ error, ...props }: any) {
  return (
    <ErrorContainer {...props }>
      <Typography color="error">Error: {error}</Typography>
    </ErrorContainer>
  );
}
