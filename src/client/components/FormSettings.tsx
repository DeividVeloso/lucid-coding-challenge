import React, { useEffect } from "react";
import { useFormik, FormikProvider, Field } from "formik";
import Button from "@material-ui/core/Button";
import styled from "styled-components";
import _shuffe from 'lodash/shuffle';

import MultipleInput from "./inputs/MultipleInput";
import SliderInput from "./inputs/SliderInput";
import { useQuestions } from "../contexts/QuestionsContext";
import { useSettings } from "../contexts/SettingsContext";
import { getQuestions } from "../services/question";
import { DIFFICULTY_OPTIONS, TYPE_QUESTIONS } from "../enums";
import ErrorMessage from "./ErrorMessage";
import Loading from "./Loading";

import { useHistory } from "react-router-dom";

const FieldContainer = styled.div`
  padding: 16px 0px;
`;

const SaveButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const difficultyOptions = [
  DIFFICULTY_OPTIONS.EASY,
  DIFFICULTY_OPTIONS.MEDIUM,
  DIFFICULTY_OPTIONS.HARD,
  DIFFICULTY_OPTIONS.RANDOM,
];
const typeOptions = [
  TYPE_QUESTIONS.BOOLEAN,
  TYPE_QUESTIONS.MULTIPLE,
  TYPE_QUESTIONS.TEXT,
  TYPE_QUESTIONS.RANDOM,
];

function FormSettings({ onClose }: any) {
  const history = useHistory();
  const { setQuestions } = useQuestions();
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState("");

  const { setSettings } = useSettings();

  const formik = useFormik({
    initialValues: {
      difficulty: DIFFICULTY_OPTIONS.RANDOM,
      type: TYPE_QUESTIONS.RANDOM,
      maxQuestions: 50,
    },
    onSubmit: (values) => {
      setSettings(values);
      history.push("/quiz");
      onClose();
    },
  });

  useEffect(() => {
    formik.resetForm();
    setSettings({
      difficulty: DIFFICULTY_OPTIONS.RANDOM,
      type: TYPE_QUESTIONS.RANDOM,
      maxQuestions: 50,
    });
    setLoading(true);
    const fetchData = async () => {
      const response = await getQuestions();
      if (response && response.success) {
        setError("");
        setQuestions(_shuffe(response.data.results));
      } else {
        setError(response.error);
      }
      setLoading(false);
    };

    fetchData();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function valueLabelFormat(value: number) {
    return value;
  }

  function valuetext(value: number) {
    return value;
  }

  function renderComponent() {
    if (error) {
      return <ErrorMessage error={error} />;
    }

    if (loading) {
      return <Loading center={true} />;
    }

    return (
      <form onSubmit={formik.handleSubmit}>
        <FormikProvider value={formik}>
          <FieldContainer>
            <Field
              options={difficultyOptions}
              id={"difficulty"}
              name={"difficulty"}
              label={"Difficulty level: "}
              value={formik.values.difficulty}
              onChange={formik.handleChange}
              component={MultipleInput}
            />
          </FieldContainer>
          <FieldContainer>
            <Field
              options={typeOptions}
              id={"type"}
              name={"type"}
              label={"Type of questions: "}
              value={formik.values.type}
              onChange={formik.handleChange}
              component={MultipleInput}
            />
          </FieldContainer>
          <FieldContainer>
            <Field
              id="maxQuestions"
              name="maxQuestions"
              component={SliderInput}
              label={"Quantity of questions: "}
              valueLabelFormat={valueLabelFormat}
              getAriaValueText={valuetext}
              aria-labelledby="discrete-slider-restrict"
              step={10}
              value={formik.values.maxQuestions}
              min={10}
              max={50}
              valueLabelDisplay="on"
            />
          </FieldContainer>

          <SaveButtonContainer>
            <Button
              data-testid="btn-save-seetings"
              type="submit"
              color="primary"
              variant="contained"
            >
              Save
            </Button>
          </SaveButtonContainer>
        </FormikProvider>
      </form>
    );
  }

  const renderedComponent = renderComponent();

  return <React.Fragment>{renderedComponent}</React.Fragment>;
}
export default FormSettings;
