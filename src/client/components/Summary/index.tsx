import React from "react";
import { useHistory } from "react-router-dom";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import styled from "styled-components";

import { useSummary } from "../../contexts/SummaryContext";
import { usePagination } from "../../contexts/PaginationContext";
import { useQuestions } from "../../contexts/QuestionsContext";

import {
  convertObjectAnswersToArray,
  removeEndDotsFromQuestions,
  getScoreQuiz,
} from "../../utils/helpers";

function Bold({ children }: any) {
  return <b>{children}</b>;
}

const TitleSummary = styled(Typography)`
  text-transform: uppercase;
  font-weight: bold;
`;

const DetailsContainer = styled.div`
  margin: 24px 0px 30px;
`;

interface IDescriptionLine {
  label: string;
  description: number | string;
}

function DescriptionLine({ label, description }: IDescriptionLine) {
  return (
    <Typography color="textPrimary">
      {label}: <Bold>{description}</Bold>
    </Typography>
  );
}

function Summary() {
  const history = useHistory();
  const {
    summary: { questionsAnswered },
    setSummary,
  } = useSummary();

  const { questions } = useQuestions();

  const { setPagination } = usePagination();

  const answers = convertObjectAnswersToArray(questionsAnswered);
  const newQuestions = removeEndDotsFromQuestions(questions);

  const { countCorrect, countWrong, countQuestionsAnswered, finalScore } =
    getScoreQuiz(answers, newQuestions);

  function handleResetQuiz() {
    setSummary({ questionsAnswered: {}, questions: [] });
    setPagination({ totalPages: 0, currentPage: 0, lastPage: false });
    history.push("/");
  }

  return (
    <div data-testid="summary">
      <TitleSummary variant="h6" color="textPrimary">
        Summary
      </TitleSummary>
      <DetailsContainer>
        <DescriptionLine label="Correct" description={countCorrect} />
        <DescriptionLine label="Wrong" description={countWrong} />
        <DescriptionLine
          label="Questions Answered"
          description={countQuestionsAnswered}
        />
        <DescriptionLine label="Final Score" description={`${finalScore}%`} />
      </DetailsContainer>
      <Button
        onClick={handleResetQuiz}
        variant="contained"
        color="primary"
      >
        Restart Quiz
      </Button>
    </div>
  );
}
export default Summary;
