import '@testing-library/jest-dom'
import React from "react";
import Summary from "./index";
import { SummaryContext } from "../../contexts/SummaryContext";
import { cleanup, render, screen } from "@testing-library/react";


describe("Summary Component", () => {
  afterEach(cleanup);

  //@ts-ignore
  const customRender = (ui, {providerProps, ...renderOptions}) => {
    return render(
      <SummaryContext.Provider {...providerProps}>{ui}</SummaryContext.Provider>,
      renderOptions,
    )
  }

  it('it should render "Summary"', () => {
    const providerProps = {
      value: {
        summary: {
          questions: [],
          questionsAnswered: {}
        }, 
        setSummary: () => {}
      }
    }
    const {debug} =  customRender(<Summary />, {providerProps})
    const title = screen.getByRole('heading', {
      name: /summary/i
    })

    const correct = screen.getByText(/correct:/i)
    const wrong = screen.getByText(/wrong:/i)
    const questionsAnswered = screen.getByText(/questions answered:/i)
    const finalScore = screen.getByText(/final score:/i)
    const btnRestart = screen.getByRole('button', {
      name: /restart quiz/i
    })

    expect(title).toHaveTextContent('Summary')
    expect(correct).toHaveTextContent('Correct: 0')
    expect(wrong).toHaveTextContent('Wrong: 0')
    expect(questionsAnswered).toHaveTextContent('Questions Answered: 0')
    expect(finalScore).toHaveTextContent('Final Score: 0%')
    expect(btnRestart).toHaveTextContent('Restart Quiz')
  });
});
