import React from "react";
import { Field } from "formik";
import Typography from "@material-ui/core/Typography";

import MultipleInput from "./inputs/MultipleInput";
import TextInput from "./inputs/TextInput";
import { TYPE_QUESTIONS } from "../enums";

export default function QuestionFactory({ data, validate, formik }: any) {
  function createInput() {
    //We need that because formmik remove \. from key/name values from the object initialValues
    const name = data.question.replace(/\.$/gm, "");

    switch (data.type) {
      case TYPE_QUESTIONS.TEXT:
        return (
          <Field
            id={name}
            name={name}
            label={data.question}
            value={formik.values[name]}
            onChange={formik.handleChange}
            component={TextInput}
            validate={validate}
          />
        );

      case TYPE_QUESTIONS.MULTIPLE:
        return (
          <Field
            options={data.options}
            id={name}
            name={name}
            label={data.question}
            value={formik.values[name]}
            onChange={formik.handleChange}
            component={MultipleInput}
            validate={validate}
          />
        );

      case TYPE_QUESTIONS.BOOLEAN:
        return (
          <Field
            options={["True", "False"]}
            id={name}
            name={name}
            label={data.question}
            value={formik.values[name]}
            onChange={formik.handleChange}
            component={MultipleInput}
            validate={validate}
          />
        );

      default:
        return (
          <Typography color="error">{`We do not support the type ${data.type}`}</Typography>
        );
    }
  }
  const input = createInput();
  return input;
}
