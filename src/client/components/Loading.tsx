import React from "react";
import styled from "styled-components";
import CircularProgress from "@material-ui/core/CircularProgress";

interface ILoading {
  center?: boolean;
}

const LoadingContainer = styled.div<ILoading>`
  display: flex;
  justify-content: center;
  min-width: 320px;
  min-height: 320px;

  ${props => props.center ? ` align-items: center;` : ''} 
`;

export default function Loading({center}: ILoading) {
  return (
    <LoadingContainer center={center}>
      <CircularProgress />
    </LoadingContainer>
  );
}
