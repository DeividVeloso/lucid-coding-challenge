import "@testing-library/jest-dom";
import React from "react";
import QuestionList from "./index";
import { QuestionsContext } from "../../contexts/QuestionsContext";
import { cleanup, render, screen } from "@testing-library/react";

describe.only("QuestionList Component", () => {
  afterEach(cleanup);

  //@ts-ignore
  const customRender = (ui, { providerProps, ...renderOptions }) => {
    return render(
      <QuestionsContext.Provider {...providerProps}>
        {ui}
      </QuestionsContext.Provider>,
      renderOptions
    );
  };

  it('it should render "QuestionList" with Error Message when array question is empty', () => {
    const providerProps = {
      value: {
        questions: [],
        setQuestions: () => {},
      },
    };

    const { debug } = customRender(<QuestionList />, { providerProps });
    const errorMessage = screen.getByText(
      /sorry, we didn't find any question with the applyed filters try to change the values and try again\./i
      )


    const btnHomePage = screen.getByRole("button", {
      name: /Go to Home Page/i,
    });

    expect(errorMessage).toHaveTextContent("Sorry, we didn't find any question with the applyed filters try to change the values and try again.")
    expect(btnHomePage).toHaveTextContent("Go to Home Page");
  
  });

  it('it should render "QuestionList" Multiple input item', () => {
    const questions = [{
      "category": "Entertainment: Video Games",
      "type": "multiple",
      "difficulty": "easy",
      "question": "Which game did &quot;Sonic The Hedgehog&quot; make his first appearance in?",
      "correct_answer": "Rad Mobile",
      "incorrect_answers": [
        "Sonic The Hedgehog",
        "Super Mario 64",
        "Mega Man"
      ]
    },]
    const providerProps = {
      value: {
        questions: questions,
        setQuestions: () => {},
      },
    };

    const { debug } = customRender(<QuestionList />, { providerProps });
    const questionTitle = screen.getByRole('heading', {
      name: /which game did "sonic the hedgehog" make his first appearance in\?/i
    })

    const strToDecode = questions[0].question;
    const parser = new DOMParser();
    const decodedString = parser.parseFromString(`<!doctype html><body>${strToDecode}`, 'text/html').body.textContent;
  
    expect(questionTitle).toHaveTextContent(decodedString!)
  });
});
