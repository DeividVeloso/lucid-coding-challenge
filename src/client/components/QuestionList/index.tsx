import React from "react";
import { useFormik, FormikProvider } from "formik";
import Button from "@material-ui/core/Button";
import styled from "styled-components";
import { useHistory } from "react-router-dom";

import { useQuestions } from "../../contexts/QuestionsContext";
import { usePagination } from "../../contexts/PaginationContext";
import { useSummary } from "../../contexts/SummaryContext";

import QuestionFactory from "../QuestionFactory";

import { IPagination, IQuestion, ISummary } from "../../interfaces";
import { Typography } from "@material-ui/core";

enum QuestionType {
  TEXT = "text",
  MULTIPLE = "multiple",
  BOOLEAN = "boolean",
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

function QuestionList() {
  const QUESTION_PER_PAGE = 1;
  const history = useHistory();
  const { questions } = useQuestions();
  const { setPagination } = usePagination();
  const { setSummary } = useSummary();

  const [pages, setPages] = React.useState(
    Math.ceil(questions.length / QUESTION_PER_PAGE)
  );
  const [currentPage, setCurrentPage] = React.useState(1);

  React.useEffect(() => {
    setPages(Math.ceil(questions.length / QUESTION_PER_PAGE));
  }, [questions, QUESTION_PER_PAGE]);

  function goToNextPage() {
    if (currentPage !== pages) {
      setCurrentPage((page) => page + 1);
    } else {
      setPagination((pagination: IPagination) => ({
        ...pagination,
        lastPage: true,
      }));
    }
  }

  const formik = useFormik({
    initialValues: {},
    onSubmit: (values) => {
      setSummary((summary: ISummary) => ({
        questionsAnswered: { ...summary.questionsAnswered, ...values },
      }));
      goToNextPage();
    },
  });

  function required(value: any) {
    let error;
    if (!value) {
      error = "Required field";
    }
    return error;
  }

  const startIndex = currentPage * QUESTION_PER_PAGE - QUESTION_PER_PAGE;
  const endIndex = startIndex + QUESTION_PER_PAGE;

  const formattedQuestions: any = questions
    .slice(startIndex, endIndex)
    .map((item: IQuestion) => {
      if (item.type === QuestionType.MULTIPLE) {
        const orderByAsc = [
          ...item.incorrect_answers,
          item.correct_answer,
        ].sort();

        return {
          ...item,
          type: item.type,
          options: orderByAsc,
        };
      }
      return item;
    });

  if (!formattedQuestions || formattedQuestions.length < 1) {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Typography color="error" gutterBottom>
          Sorry, we didn't find any question with the applyed filters try to
          change the values and try again.{" "}
        </Typography>
        <Button
          color="primary"
          variant="outlined"
          onClick={() => history.push("/")}
        >
          Go to Home Page
        </Button>
      </div>
    );
  }

  return (
    <Container data-testid='question-list'>
      <form onSubmit={formik.handleSubmit}>
        <FormikProvider value={formik}>
          {formattedQuestions.map((item: any, index: number) => {
            return (
              <Container key={index}>
                <QuestionFactory
                  data={item}
                  validate={required}
                  formik={formik}
                />

                <Button
                  data-testid="question-list-btn-next"
                  style={{ marginTop: 30 }}
                  variant="contained"
                  color="primary"
                  type="submit"
                >
                  Next
                </Button>
              </Container>
            );
          })}
        </FormikProvider>
      </form>
    </Container>
  );
}
export default QuestionList;
