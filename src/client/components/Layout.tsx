import React from "react";
import styled from "styled-components";

interface ILayout {
  children: JSX.Element | JSX.Element[];
}

const Container = styled.main`
  margin: 0px auto;
  max-width: 1200px;
`;

function Layout({ children }: ILayout) {
  return <Container>{children}</Container>;
}
export default Layout;
