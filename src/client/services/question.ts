const QUESTION_API = "http://localhost:4000/api/questions";

interface IResponse {
  success: boolean;
  data?: any;
  error?: any;
}

function getQuestions(): Promise<IResponse> {
  return fetch(QUESTION_API, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((data) => {
      return {
        success: true,
        data,
      };
    })
    .catch((error) => {
      return {
        success: false,
        error: error.message,
      };
    });
}

export { getQuestions };
