import React from "react";
import { ISummary } from '../interfaces';

export type SummaryType = {
  summary: ISummary;
  setSummary: any;
};

const SummaryContext = React.createContext<SummaryType>({
  summary: {},
  setSummary: () => {},
});

function useSummary() {
  const context = React.useContext(SummaryContext);
  if (context === undefined) {
    throw new Error(
      "useSummary must be use with a SummaryContext provider"
    );
  }
  return context;
}
export { SummaryContext, useSummary };
