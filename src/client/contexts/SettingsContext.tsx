import React from "react";
import { ISettings } from '../interfaces';

export type SettingsType = {
  settings: ISettings;
  setSettings: any;
};

const SettingsContext = React.createContext<SettingsType>({
  settings: {},
  setSettings: () => {},
});

function useSettings() {
  const context = React.useContext(SettingsContext);
  if (context === undefined) {
    throw new Error(
      "useSettings must be use with a SettingsContext provider"
    );
  }
  return context;
}
export { SettingsContext, useSettings };
