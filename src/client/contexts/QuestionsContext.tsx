import React from "react";
import { IQuestion } from '../interfaces';

export type QuestionsType = {
  questions: Array<IQuestion>;
  setQuestions: any;
};

const QuestionsContext = React.createContext<QuestionsType>({
  questions: [],
  setQuestions: () => {},
});

function useQuestions() {
  const context = React.useContext(QuestionsContext);
  if (context === undefined) {
    throw new Error(
      "useQuestions must be use with a QuestionsContext provider"
    );
  }
  return context;
}
export { QuestionsContext, useQuestions };
