import React from "react";
import { IPagination } from '../interfaces';

export type PaginationType = {
  pagination: IPagination;
  setPagination: any;
};

const PaginationContext = React.createContext<PaginationType>({
  pagination: {},
  setPagination: () => {},
});

function usePagination() {
  const context = React.useContext(PaginationContext);
  if (context === undefined) {
    throw new Error(
      "usePagination must be use with a PaginationContext provider"
    );
  }
  return context;
}
export { PaginationContext, usePagination };
