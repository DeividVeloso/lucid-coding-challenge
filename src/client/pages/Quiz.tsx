import React from "react";
import QuestionList from "../components/QuestionList";
import Summary from "../components/Summary";
import { PaginationContext } from "../contexts/PaginationContext";
import { SummaryContext } from "../contexts/SummaryContext";
import { ISummary, IPagination } from "../interfaces";

export default function Quiz() {
  const [summary, setSummary] = React.useState<ISummary>({
    questions: [],
    questionsAnswered: {},
  });

  const [pagination, setPagination] = React.useState<IPagination>({
    totalPages: 0,
    currentPage: 0,
    lastPage: false,
  });

  return (
    <PaginationContext.Provider value={{ pagination, setPagination }}>
      <SummaryContext.Provider value={{ summary, setSummary }}>
        {!pagination.lastPage ? (
          <QuestionList></QuestionList>
        ) : (
          <Summary></Summary>
        )}
      </SummaryContext.Provider>
    </PaginationContext.Provider>
  );
}
