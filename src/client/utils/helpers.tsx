import { IQuestion } from '../interfaces';

export function convertObjectAnswersToArray(answersObject: any) {
  return Object.keys(answersObject).map((item) => {
    let value = answersObject[item];
    if (typeof answersObject[item] === "object") {
      value = Object.values(answersObject[item])[0];
    }

    return {
      question: item,
      answered: value,
    };
  });
}

export function removeEndDotsFromQuestions(questions: Array<IQuestion>) {
  return questions.map((item: any) => {
    let questionTitle = item.question;
    if (item.question.match(/\.$/gm)) {
      questionTitle = item.question.replace(/\.$/gm, "");
    }

    return {
      ...item,
      question: questionTitle,
    };
  });
}

export function getScoreQuiz(answers: any, questions: any) {
  let countCorrect = 0;
  let countWrong = 0;
  let countQuestionsAnswered = 0;
  let finalScore = 0;

  answers.forEach((answer: any) => {
    questions.forEach((q: IQuestion) => {
      if (
        answer.question === q.question &&
        answer.answered === q.correct_answer
      ) {
        countCorrect = countCorrect + 1;
      } else if (
        answer.question === q.question &&
        answer.answered !== q.correct_answer
      ) {
        countWrong = countWrong + 1;
      }
    });
  });

  if (answers && answers.length) {
    countQuestionsAnswered = answers.length;
  }

  if (countCorrect > 0 && countQuestionsAnswered > 0) {
    finalScore = Math.floor((countCorrect / countQuestionsAnswered) * 100);
  }

  return {
    countCorrect,
    countWrong,
    countQuestionsAnswered,
    finalScore,
  };
}

