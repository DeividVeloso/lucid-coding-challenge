import {
  convertObjectAnswersToArray,
  removeEndDotsFromQuestions,
  getScoreQuiz,
} from "./helpers";

describe("Helper", () => {
  it("should convert an Object to Array", () => {
    const answeredQuestions = {
      "Which game did &quot;Sonic The Hedgehog&quot; make his first appearance in?":
        "Rad Mobile",
      "Igneous rocks are formed by excessive heat and pressure.": "False",
    };

    const answeredArray = convertObjectAnswersToArray(answeredQuestions);
    expect(answeredArray).toHaveLength(2);
    expect(answeredArray[1].question).toEqual(
      "Igneous rocks are formed by excessive heat and pressure."
    );
    expect(answeredArray[1].answered).toEqual("False");
  });

  it("should remove the full stop in the end of question title", () => {
    const questions = [
      {
        category: "Science & Nature",
        type: "boolean",
        difficulty: "easy",
        question: "Igneous rocks are formed by excessive heat and pressure.",
        correct_answer: "False",
        incorrect_answers: ["True"],
      },
    ];

    const newQuestions = removeEndDotsFromQuestions(questions);
    expect(newQuestions[0].question).not.toEqual(questions[0].question);
    expect(newQuestions[0].question).toMatch(/\b$/gm);
  });

  it("should return the score of the quiz", () => {
    const answeredQuestions = {
      "Which game did &quot;Sonic The Hedgehog&quot; make his first appearance in?":
        "Rad Mobile",
      "Igneous rocks are formed by excessive heat and pressure": "False",
    };

    const answeredArray = convertObjectAnswersToArray(answeredQuestions);
    const questions = [
      {
        category: "Science & Nature",
        type: "boolean",
        difficulty: "easy",
        question: "Igneous rocks are formed by excessive heat and pressure.",
        correct_answer: "False",
        incorrect_answers: ["True"],
      },
      {
        category: "Entertainment: Video Games",
        type: "multiple",
        difficulty: "easy",
        question:
          "Which game did &quot;Sonic The Hedgehog&quot; make his first appearance in?",
        correct_answer: "Rad Mobile",
        incorrect_answers: ["Sonic The Hedgehog", "Super Mario 64", "Mega Man"],
      },
    ];

    const newQuestions = removeEndDotsFromQuestions(questions);
    const score = getScoreQuiz(answeredArray, newQuestions);
    expect(score.countCorrect).toEqual(2);
    expect(score.countWrong).toEqual(0);
    expect(score.countQuestionsAnswered).toEqual(2);
    expect(score.finalScore).toEqual(100);
  });
});
