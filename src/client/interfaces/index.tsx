export interface ISettings {
  difficulty?: string;
  type?: string;
  maxQuestions?: number;
}

export interface IQuestion {
  category?: string;
  type?: string;
  difficulty?: string;
  question?: string;
  correct_answer?: string;
  incorrect_answers: Array<string>;
}

export interface ISummary {
  questions?: Array<IQuestion>,
  questionsAnswered?: any,
}

export interface IPagination {
  totalPages?: number,
  currentPage?: number,
  lastPage?: boolean,
}
