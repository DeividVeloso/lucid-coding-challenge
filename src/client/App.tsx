import React, { useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import _shuffe from 'lodash/shuffle';

import Layout from "./components/Layout";
import Container from "./components/Container";

import { QuestionsContext } from "./contexts/QuestionsContext";
import { SettingsContext } from "./contexts/SettingsContext";

import { getQuestions } from "./services/question";
import Home from "./pages/Home";
import Quiz from "./pages/Quiz";
import { IQuestion, ISettings } from "./interfaces";
import { DIFFICULTY_OPTIONS, TYPE_QUESTIONS } from "./enums";
import ErrorMessage from "./components/ErrorMessage";
import Loading from './components/Loading';

export function App() {
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState("");

  const [questions, setQuestions] = React.useState<Array<IQuestion>>([]);
  const [settings, setSettings] = React.useState<ISettings>({});

  function settingQuestions(questions: Array<IQuestion>) {
    let filteredQuestions = questions;
    if (!settings) {
      return filteredQuestions;
    }

    if (settings.difficulty !== DIFFICULTY_OPTIONS.RANDOM) {
      filteredQuestions = filteredQuestions.filter(
        (item: ISettings) => item.difficulty === settings.difficulty
      );
    }

    if (settings.type !== TYPE_QUESTIONS.RANDOM) {
      filteredQuestions = filteredQuestions.filter(
        (item: ISettings) => item.type === settings.type
      );
    }

    if (settings.maxQuestions !== questions.length) {
      filteredQuestions = filteredQuestions.slice(0, settings.maxQuestions);
    }

    return filteredQuestions;
  }

  useEffect(() => {
    setLoading(true);
    const fetchData = async () => {
      const response = await getQuestions();
      if (response && response.success) {
        setError("");
        setQuestions(_shuffe(response.data.results));
      } else {
        setError(response.error);
      }
      setLoading(false);
    };
    setTimeout(() => {
      fetchData();
    }, 1000);
  }, []);

  useEffect(() => {
    const filteredQuestions = settingQuestions(questions);
    setQuestions(filteredQuestions);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [settings]);

  function renderComponent() {
    if (error) {
      return <ErrorMessage data-testid="app-error-message" error={error} />;
    }
    if (loading) {
      return <Loading />;
    }
    return (
      <QuestionsContext.Provider value={{ questions, setQuestions }}>
        <SettingsContext.Provider value={{ settings, setSettings }}>
          <Router>
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              <Route exact path="/quiz">
                <Quiz />
              </Route>
            </Switch>
          </Router>
        </SettingsContext.Provider>
      </QuestionsContext.Provider>
    );
  }

  const renderedComponent = renderComponent();

  return (
    <React.Fragment>
      <Typography
        variant="h4"
        component="h1"
        color="textPrimary"
        align="center"
        style={{ padding: "16px 0px" }}
      >
        Welcome to the Lucid Quiz
      </Typography>
      <Divider style={{ width: "100%" }} />
      <Layout>
        <Container>{renderedComponent}</Container>
      </Layout>
    </React.Fragment>
  );
}
