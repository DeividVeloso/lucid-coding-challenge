
export enum DIFFICULTY_OPTIONS {
  EASY = "easy",
  MEDIUM = "medium",
  HARD = "hard",
  RANDOM = "random",
} 

export enum TYPE_QUESTIONS {
  TEXT = "text",
  MULTIPLE = "multiple",
  BOOLEAN = "boolean",
  RANDOM = "random",
}
