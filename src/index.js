import React from "react";
import ReactDOM from "react-dom";
import CssBaseline from "@material-ui/core/CssBaseline";

import { createTheme, ThemeProvider } from "@material-ui/core/styles";

import "./index.css";
import { App } from "./client/App";

const theme = createTheme({
  palette: {
    primary: {
      main: "#1E70DD",
    },
    text: {
      primary: "#444951",
    },
  },
  overrides: {
    MuiButton: {
      root: {
        borderRadius: 0,
        textTransform: 'capitalize',
      },
    },
  },
});

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <App />
  </ThemeProvider>,
  document.getElementById("root")
);
